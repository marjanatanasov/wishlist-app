$(".deleteitem").click(function() {
    if(confirm("Are you sure you want to delete this item?")) {
        const wishlistId = $(this).data("wishlistid");
        const wishlistItemId = $(this).data("wishlistitemid");
        const token = $(this).data("token");
        $.ajax(
            {
                url: "/wishlists/" + wishlistId + "/items/" + wishlistItemId,
                type: 'DELETE',
                dataType: "JSON",
                data: {
                    "id": wishlistItemId,
                    "_token": token,
                },
                success: function () {
                    $("#listtr-" + wishlistItemId).fadeOut();
                }
            }
        );
    }
});

$('#name').keyup(delay(function (e) {
    const term = $(this).val() ?? '';
    $.ajax(
        {
            url: "/imgur/search/" + term,
            type: 'GET',
            success: function (response) {
                if(response.status == 'OK')
                {
                    $('#imgurImagesWrapper').html('');
                    response.images.forEach(image => {
                        $('#imgurImagesWrapper').append('<div class="col-lg-3 col-sm-6"><img src="' + image + '" alt="Imgur img" class="imgur-img"/></div>');
                    })
                    initImgurClick();
                }
            }
        }
    );
}, 500));

function delay(fn, ms) {
    let timer = 0
    return function(...args) {
        clearTimeout(timer)
        timer = setTimeout(fn.bind(this, ...args), ms || 0)
    }
}

function initImgurClick()
{
    $(".imgur-img").unbind();
    $(".imgur-img").bind('click', function() {
        $('.imgur-img').each(function() { $(this).removeClass('active'); });

        $(this).addClass('active');
        $('#imgur_image').val($(this).attr('src'));
    });
}
