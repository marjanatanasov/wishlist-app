$(".deletewishlist").click(function() {
    if(confirm("Are you sure you want to delete this wishlist?")) {
        var id = $(this).data("id");
        var token = $(this).data("token");
        $.ajax(
            {
                url: "/wishlists/" + id,
                type: 'DELETE',
                dataType: "JSON",
                data: {
                    "id": id,
                    "_token": token,
                },
                success: function () {
                    $("#listtr-" + id).fadeOut();
                }
            }
        );
    }
});
$(".deleteshare").click(function() {
    if(confirm("Are you sure you want to stop sharing the wishlist with this user?")) {
        var wishlistid = $(this).data("wishlistid");
        var id = $(this).data("id");
        var token = $(this).data("token");
        $.ajax(
            {
                url: "/wishlists/" + wishlistid + '/shares/' + id,
                type: 'DELETE',
                dataType: "JSON",
                data: {
                    "id": id,
                    "_token": token,
                },
                success: function () {
                    $("#listtr-" + id).fadeOut();
                }
            }
        );
    }
});
