<?php

namespace App\Traits;

use Illuminate\Support\Facades\Cache;

trait CacheManager
{
    public function getCachedItem($key)
    {
        if (!Cache::has($key)) {
            return false;
        }
        return Cache::get($key);
    }

    public function setCacheItem($key, $value)
    {
        Cache::put($key, $value, config('cache.timeout'));
    }
}
