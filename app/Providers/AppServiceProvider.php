<?php

namespace App\Providers;

use App\Models\Wishlist;
use App\Observers\WishlistObserver;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Wishlist::observe(WishlistObserver::class);
        Paginator::useBootstrap();
    }
}
