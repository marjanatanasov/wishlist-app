<?php

namespace App\Providers;

use App\Interfaces\UserRepositoryInterface;
use App\Interfaces\WishlistItemRepositoryInterface;
use App\Interfaces\WishlistRepositoryInterface;
use App\Interfaces\WishlistShareRepositoryInterface;
use App\Repositories\UserRepository;
use App\Repositories\WishlistItemRepository;
use App\Repositories\WishlistRepository;
use App\Repositories\WishlistShareRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(WishlistRepositoryInterface::class, WishlistRepository::class);
        $this->app->bind(WishlistItemRepositoryInterface::class, WishlistItemRepository::class);
        $this->app->bind(WishlistShareRepositoryInterface::class, WishlistShareRepository::class);
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
