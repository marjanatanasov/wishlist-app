<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Storage;

class S3Helper
{
    public static function uploadFile($theFile)
    {
        $extension = $theFile->getClientOriginalExtension();
        $amazon_fname = 'image_' . time() . '.' . $extension;
        $filename = auth()->user()->id . "/" . $amazon_fname;

        Storage::disk('s3')->put($filename, file_get_contents($theFile->getPathName()));
        Storage::disk('s3')->setVisibility($filename, 'public');

        return Storage::disk('s3')->url($filename);
    }
}
