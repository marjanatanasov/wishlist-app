<?php

namespace App\Helpers;

use App\Http\API\ImgurAPI;
use App\Traits\CacheManager;

class ImgurHelper
{
    use CacheManager;

    public static function gallerySearch($term)
    {
        if ($result = (new ImgurHelper)->getCachedItem('imgur_' . $term)) {
            return $result;
        }

        $imgurAPI = new ImgurAPI();
        $response = $imgurAPI->get('gallery/search?q_all=' . urlencode($term));
        if (!$response) return [];

        $collection = collect($response['data'])->map(function ($item) {
            if (isset($item['images'])) {
                return $item['images'][0]['link'];
            }
            return null;
        })->reject(function ($item) {
            return empty($item) || !in_array(pathinfo($item, PATHINFO_EXTENSION), ['jpg', 'gif']);
        });
        $images = count($collection) > 4 ? $collection->random(4) : $collection;

        (new ImgurHelper)->setCacheItem('imgur_' . $term, $images);

        return $images;
    }
}
