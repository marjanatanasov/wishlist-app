<?php

namespace App\Helpers;

use App\Models\Wishlist;
use Illuminate\Support\Str;

class WishlistHelper
{
    public static function generateSlug($name)
    {
        $slug = Str::slug($name);
        $i = 1;
        $exists = Wishlist::where('slug', $slug)->first();
        while ($exists) {
            $slug = Str::slug($name . " " . $i);
            $exists = Wishlist::where('slug', $slug)->first();
            $i++;
        }
        return $slug;
    }
}
