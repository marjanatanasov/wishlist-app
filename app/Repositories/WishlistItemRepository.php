<?php

namespace App\Repositories;

use App\Interfaces\WishlistItemRepositoryInterface;
use App\Models\WishlistItem;

class WishlistItemRepository implements WishlistItemRepositoryInterface
{
    public function getWishlistItems($wishlistId)
    {
        return WishlistItem::query()->where('wishlist_id', $wishlistId)->paginate(10);
    }

    public function getWishlistItem($wishlistItemId)
    {
        return WishlistItem::query()->findOrFail($wishlistItemId);
    }

    public function createWishlistItem(array $data)
    {
        return WishlistItem::query()->create($data);
    }

    public function updateWishlistItem($wishlistItemId, $data)
    {
        $wishlistItem = WishlistItem::findOrFail($wishlistItemId);
        $wishlistItem->fill($data);
        return $wishlistItem->save();
    }

    public function deleteWishlistItem($wishlistItemId)
    {
        return WishlistItem::destroy($wishlistItemId);
    }
}
