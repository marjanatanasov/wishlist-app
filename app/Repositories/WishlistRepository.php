<?php

namespace App\Repositories;

use App\Interfaces\WishlistRepositoryInterface;
use App\Models\Wishlist;
use App\Models\WishlistShare;

class WishlistRepository implements WishlistRepositoryInterface
{

    public function getWishlistsForUser($userId)
    {
        return Wishlist::query()->withCount('items')->where('user_id', $userId)->get();
    }

    public function getSharedWishlistsForUser($userId)
    {
        return WishlistShare::query()->with(['wishlist.user', 'user'])->where('user_id', $userId)->get();
    }

    public function getWishlist($wishlistId)
    {
        return Wishlist::findOrFail($wishlistId);
    }

    public function getWishlistBySlug($slug)
    {
        return Wishlist::where('slug', $slug)->first();
    }

    public function getWishlistWithItemsBySlug($slug)
    {
        return Wishlist::with('items')->where('slug', $slug)->first();
    }

    public function createWishlist(array $data)
    {
        return Wishlist::query()->create($data);
    }

    public function updateWishlist($wishlistId, $data)
    {
        $wishlist = Wishlist::findOrFail($wishlistId);
        $wishlist->fill($data);
        return $wishlist->save();
    }

    public function deleteWishlist($wishlistId)
    {
        return Wishlist::destroy($wishlistId);
    }
}
