<?php

namespace App\Repositories;

use App\Interfaces\UserRepositoryInterface;
use App\Models\User;

class UserRepository implements UserRepositoryInterface
{

    public function getUserByEmail($email)
    {
        return User::query()->where('email', $email)->first();
    }
}
