<?php

namespace App\Repositories;

use App\Interfaces\WishlistShareRepositoryInterface;
use App\Models\WishlistShare;

class WishlistShareRepository implements WishlistShareRepositoryInterface
{
    public function createWishlistShare(array $data)
    {
        return WishlistShare::query()->create($data);
    }

    public function deleteWishlistShare($wishlistShareId)
    {
        return WishlistShare::destroy($wishlistShareId);
    }
}
