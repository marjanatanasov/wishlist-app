<?php

namespace App\Observers;

use App\Helpers\WishlistHelper;
use App\Models\Wishlist;

class WishlistObserver
{
    /**
     * Handle the Wishlist "saved" event.
     *
     * @param Wishlist $wishlist
     * @return void
     */
    public function saved(Wishlist $wishlist)
    {
        if ($wishlist->isDirty('name')) {
            $slug = WishlistHelper::generateSlug($wishlist->name);
            $wishlist->slug = $slug;
            $wishlist->saveQuietly();
        }
    }
}
