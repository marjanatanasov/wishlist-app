<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WishlistShare extends Model
{
    use HasFactory;

    protected $fillable = [
        'wishlist_id',
        'user_id',
        'email'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function wishlist()
    {
        return $this->belongsTo(Wishlist::class);
    }
}
