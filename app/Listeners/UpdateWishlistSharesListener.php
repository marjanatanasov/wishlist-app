<?php

namespace App\Listeners;

use App\Models\WishlistShare;
use Illuminate\Auth\Events\Registered;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateWishlistSharesListener implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param Registered $event
     * @return void
     */
    public function handle(Registered $event)
    {
        $user = $event->user;
        WishlistShare::where('email', $user->email)->whereNull('user_id')->update(['user_id' => $user->id]);
    }
}
