<?php

namespace App\Http\Responses;

class SuccessDataResponse
{


    public static function return($data = [], $status = 200)
    {
        $response_data = [
            "status" => "OK"
        ];
        return response()->json($response_data + $data, $status);
    }
}
