<?php

namespace App\Http\Responses;

class SuccessMessageResponse
{


    public static function return($message = '', $status = 200)
    {
        $response_data = [
            "status" => "OK"
        ];
        if (!empty($message)) {
            $response_data['message'] = $message;
        }
        return response()->json($response_data, $status);
    }
}
