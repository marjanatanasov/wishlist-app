<?php

namespace App\Http\Responses;

class InvalidResponse
{

    public static function return($data = [], $status = 422)
    {
        $response = [
            "status" => "ERROR"
        ];
        if (!empty($data)) {
            $response['data'] = $data;
        }
        return response()->json($response, $status);
    }
}
