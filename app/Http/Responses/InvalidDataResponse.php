<?php

namespace App\Http\Responses;

class InvalidDataResponse
{


    public static function return($data = [], $status = 422)
    {
        return response()->json(
            [
                "message" => "The given data was invalid.",
                "errors" => $data
            ], $status);
    }
}
