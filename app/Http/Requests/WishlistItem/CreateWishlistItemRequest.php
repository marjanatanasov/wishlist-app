<?php


namespace App\Http\Requests\WishlistItem;

use Illuminate\Foundation\Http\FormRequest;

class CreateWishlistItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'description' => 'sometimes|nullable|string',
            'price' => 'sometimes|nullable|numeric',
            'url' => 'sometimes|nullable|url',
            'imgur_image' => 'sometimes|nullable|url',
            'image' => 'sometimes|nullable|mimes:jpg,gif,png'
        ];
    }
}
