<?php


namespace App\Http\Requests\WishlistShare;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateWishlistShareRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => [
                'required',
                'email',
                Rule::unique('wishlist_shares', 'email')->where(function ($query) {
                    return $query->where('wishlist_id', $this->route('wishlist')->id);
                }),
            ]
        ];
    }

    public function messages()
    {
        return [
            'email.unique' => 'The wishlist has already been shared with this email.',
        ];
    }
}
