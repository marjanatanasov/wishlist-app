<?php

namespace App\Http\API;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;

class ImgurAPI
{
    private string $baseUrl = 'https://api.imgur.com/3/';
    private string $clientId;

    public function __construct()
    {
        $this->clientId = config('imgur.client_id');
    }

    public function get(string $uri)
    {
        $response = Http::withHeaders([
            'Authorization' => 'Client-ID ' . $this->clientId
        ])->get($this->baseUrl . $uri);

        if ($response->status() === Response::HTTP_OK) {
            return $response->json();
        }

        return false;
    }

    public function post(string $uri, array $options)
    {
        $response = Http::withHeaders([
            'Authorization' => 'Client-ID ' . $this->clientId
        ])->post($this->baseUrl . $uri, $options);

        if ($response->status() === Response::HTTP_OK) {
            return $response->json();
        }

        return false;
    }
}
