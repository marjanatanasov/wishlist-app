<?php

namespace App\Http\Controllers;

use App\Http\Middleware\CheckIfWishlistBelongsToLoggedUser;
use App\Http\Requests\Wishlist\CreateWishlistRequest;
use App\Http\Requests\Wishlist\UpdateWishlistRequest;
use App\Http\Responses\SuccessMessageResponse;
use App\Interfaces\WishlistItemRepositoryInterface;
use App\Interfaces\WishlistRepositoryInterface;
use App\Models\Wishlist;
use Illuminate\Http\Response;

class WishlistController extends Controller
{
    private WishlistRepositoryInterface $wishlistRepository;
    private WishlistItemRepositoryInterface $wishlistItemRepository;

    public function __construct(WishlistRepositoryInterface $wishlistRepository, WishlistItemRepositoryInterface $wishlistItemRepository)
    {
        $this->wishlistRepository = $wishlistRepository;
        $this->wishlistItemRepository = $wishlistItemRepository;
        $this->middleware(CheckIfWishlistBelongsToLoggedUser::class)->except('index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $wishlists = $this->wishlistRepository->getWishlistsForUser(auth()->user()->id);
        return view('user.wishlists.index', ['wishlists' => $wishlists]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('user.wishlists.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateWishlistRequest $request
     * @return Response
     */
    public function store(CreateWishlistRequest $request)
    {
        $data = array_merge($request->validated(), ['user_id' => auth()->user()->id]);
        $this->wishlistRepository->createWishlist($data);
        return redirect('wishlists');
    }

    /**
     * Display the specified resource.
     *
     * @param Wishlist $wishlist
     * @return Response
     */
    public function show(Wishlist $wishlist)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Wishlist $wishlist
     * @return Response
     */
    public function edit(Wishlist $wishlist)
    {
        $wishlist->load('shares.user');
        $wishlistItems = $this->wishlistItemRepository->getWishlistItems($wishlist->id);
        return view('user.wishlists.edit', ['wishlist' => $wishlist, 'wishlistItems' => $wishlistItems]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateWishlistRequest $request
     * @param int $id
     * @return Response
     */
    public function update(UpdateWishlistRequest $request, $id)
    {
        $this->wishlistRepository->updateWishlist($id, $request->validated());
        return redirect(route('wishlists.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Wishlist $wishlist
     * @return Response
     */
    public function destroy(Wishlist $wishlist)
    {
        $this->wishlistRepository->deleteWishlist($wishlist->id);
        return SuccessMessageResponse::return('Item deleted');
    }
}
