<?php

namespace App\Http\Controllers;

use App\Helpers\S3Helper;
use App\Http\Middleware\CheckIfWishlistBelongsToLoggedUser;
use App\Http\Requests\WishlistItem\CreateWishlistItemRequest;
use App\Http\Responses\SuccessMessageResponse;
use App\Interfaces\WishlistItemRepositoryInterface;
use App\Models\Wishlist;
use App\Models\WishlistItem;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class WishlistItemController extends Controller
{
    private WishlistItemRepositoryInterface $wishlistItemRepository;

    public function __construct(WishlistItemRepositoryInterface $wishlistItemRepository)
    {
        $this->wishlistItemRepository = $wishlistItemRepository;
        $this->middleware(CheckIfWishlistBelongsToLoggedUser::class)->except('index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Wishlist $wishlist)
    {
        return view('user.wishlists.items.create', ['wishlist' => $wishlist, 'imgurImages' => []]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateWishlistItemRequest $request
     * @param Wishlist $wishlist
     * @return Response
     */
    public function store(CreateWishlistItemRequest $request, Wishlist $wishlist)
    {
        $image = null;
        if (!empty($request->get('imgur_image'))) {
            $image = $request->get('imgur_image');
        } else if ($request->hasFile('image')) {
            $image = S3Helper::uploadFile($request->file('image'));
        }
        $data = array_merge($request->except('image', 'imgur_image'), ['wishlist_id' => $wishlist->id, 'image' => $image]);
        $this->wishlistItemRepository->createWishlistItem($data);
        return redirect('wishlists/' . $wishlist->id . '/edit');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Wishlist $wishlist
     * @param WishlistItem $item
     * @return Response
     */
    public function edit(Wishlist $wishlist, WishlistItem $item)
    {
        return view('user.wishlists.items.edit', ['wishlist' => $wishlist, 'wishlistItem' => $item]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Wishlist $wishlist
     * @param WishlistItem $item
     * @return Response
     */
    public function update(Request $request, Wishlist $wishlist, WishlistItem $item)
    {
        $image = $item->image;
        if (!empty($request->get('imgur_image'))) {
            $image = $request->get('imgur_image');
        } else if ($request->hasFile('image')) {
            $image = S3Helper::uploadFile($request->file('image'));
        }
        $data = array_merge($request->except('image', 'imgur_image'), ['image' => $image]);
        $this->wishlistItemRepository->updateWishlistItem($item->id, $data);
        return redirect(route('wishlists.edit', $wishlist->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Wishlist $wishlist
     * @param int $id
     * @return Response
     */
    public function destroy(Wishlist $wishlist, $id)
    {
        $this->wishlistItemRepository->deleteWishlistItem($id);
        return SuccessMessageResponse::return('Item deleted');
    }
}
