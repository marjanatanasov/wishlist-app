<?php

namespace App\Http\Controllers;

use App\Helpers\ImgurHelper;
use App\Http\Responses\SuccessDataResponse;

class ImgurSearchController extends Controller
{
    public function index($term = false)
    {
        if (!$term) {
            return SuccessDataResponse::return(['images' => []]);
        }
        $imgurImages = ImgurHelper::gallerySearch($term);
        return SuccessDataResponse::return(['images' => $imgurImages]);
    }
}
