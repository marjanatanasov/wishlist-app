<?php

namespace App\Http\Controllers;

use App\Http\Middleware\CheckIfWishlistBelongsToLoggedUser;
use App\Http\Requests\WishlistShare\CreateWishlistShareRequest;
use App\Http\Responses\SuccessMessageResponse;
use App\Interfaces\UserRepositoryInterface;
use App\Interfaces\WishlistShareRepositoryInterface;
use App\Mail\SharedWishlistMail;
use App\Models\Wishlist;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;

class WishlistShareController extends Controller
{
    private WishlistShareRepositoryInterface $wishlistShareRepository;
    private UserRepositoryInterface $userRepository;

    public function __construct(WishlistShareRepositoryInterface $wishlistShareRepository, UserRepositoryInterface $userRepository)
    {
        $this->wishlistShareRepository = $wishlistShareRepository;
        $this->userRepository = $userRepository;
        $this->middleware(CheckIfWishlistBelongsToLoggedUser::class)->except('index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Wishlist $wishlist)
    {
        return view('user.wishlists.shares.create', ['wishlist' => $wishlist]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateWishlistShareRequest $request
     * @param Wishlist $wishlist
     * @return Response
     */
    public function store(CreateWishlistShareRequest $request, Wishlist $wishlist)
    {
        $user = $this->userRepository->getUserByEmail($request->get('email'));
        if ($user && $user->id == auth()->user()->id) {
            return redirect(route('wishlists.edit', $wishlist->id));
        }
        $user_id = $user ? $user->id : null;
        $result = $this->wishlistShareRepository->createWishlistShare(array_merge($request->validated(), ['wishlist_id' => $wishlist->id, 'user_id' => $user_id]));
        if ($result) {
            Mail::to($request->get('email'))->send(new SharedWishlistMail($wishlist));
        }
        return redirect(route('wishlists.edit', $wishlist->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy(Wishlist $wishlist, $id)
    {
        $this->wishlistShareRepository->deleteWishlistShare($id);
        return SuccessMessageResponse::return('Share deleted');
    }
}
