<?php

namespace App\Http\Controllers;

use App\Http\Middleware\CheckIfSharedWishlistBelongsToLoggedUser;
use App\Http\Middleware\CheckIfSharedWishlistItemBelongsToLoggedUser;
use App\Interfaces\WishlistItemRepositoryInterface;
use App\Interfaces\WishlistRepositoryInterface;
use App\Mail\BoughtWishlistItemMail;
use App\Models\WishlistItem;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;

class SharedWishlistsController extends Controller
{
    private WishlistRepositoryInterface $wishlistRepository;
    private WishlistItemRepositoryInterface $wishlistItemRepository;

    public function __construct(WishlistRepositoryInterface $wishlistRepository, WishlistItemRepositoryInterface $wishlistItemRepository)
    {
        $this->wishlistRepository = $wishlistRepository;
        $this->wishlistItemRepository = $wishlistItemRepository;
        $this->middleware(CheckIfSharedWishlistBelongsToLoggedUser::class)->only('show');
        $this->middleware(CheckIfSharedWishlistItemBelongsToLoggedUser::class)->only('viewItem', 'buyItem');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $sharedWishlists = $this->wishlistRepository->getSharedWishlistsForUser(auth()->user()->id);
        return view('user.shared.index', ['wishlistShares' => $sharedWishlists]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $wishlist = $this->wishlistRepository->getWishlistWithItemsBySlug($id);
        if (!$wishlist) {
            return redirect(route('shared-wishlists.index'));
        }
        return view('user.shared.show', ['wishlist' => $wishlist]);
    }

    public function viewItem(WishlistItem $wishlistItem)
    {
        $wishlistItem = $this->wishlistItemRepository->getWishlistItem($wishlistItem->id);
        return view('user.shared.viewItem', ['wishlistItem' => $wishlistItem]);
    }

    public function buyItem(WishlistItem $wishlistItem)
    {
        $result = $this->wishlistItemRepository->updateWishlistItem($wishlistItem->id, ['is_bought' => true, 'bought_by' => auth()->user()->id]);
        if ($result) {
            Mail::to(auth()->user()->email)->send(new BoughtWishlistItemMail($wishlistItem));
        }
        return redirect(route('shared-wishlists.show', $wishlistItem->wishlist->slug));
    }


}
