<?php

namespace App\Http\Middleware;

use App\Interfaces\WishlistRepositoryInterface;
use App\Models\WishlistShare;
use Closure;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class CheckIfSharedWishlistBelongsToLoggedUser
{
    private $wishlistRepository;

    public function __construct(WishlistRepositoryInterface $wishlistRepository)
    {
        $this->wishlistRepository = $wishlistRepository;
    }

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return Response|RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $wishlist = $this->wishlistRepository->getWishlistBySlug(request()->route('shared_wishlist'));
        if ($wishlist->user_id == Auth::user()->id) {
            return redirect('dashboard');
        }
        if (!WishlistShare::where('wishlist_id', $wishlist->id)->where('user_id', Auth::user()->id)->exists()) {
            return redirect('dashboard');
        }
        return $next($request);
    }
}
