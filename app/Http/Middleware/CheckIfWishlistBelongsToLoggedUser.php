<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class CheckIfWishlistBelongsToLoggedUser
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return Response|RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $wishlist = request()->route('wishlist');
        if (!$wishlist) {
            return $next($request);
        }
        if ($wishlist->user_id != Auth::user()->id) {
            return redirect('dashboard');
        }
        return $next($request);
    }
}
