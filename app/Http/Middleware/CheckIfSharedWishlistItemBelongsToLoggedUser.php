<?php

namespace App\Http\Middleware;

use App\Models\WishlistShare;
use Closure;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class CheckIfSharedWishlistItemBelongsToLoggedUser
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return Response|RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $wishlistItem = request()->route('wishlistItem');
        if (!WishlistShare::where('wishlist_id', $wishlistItem->wishlist_id)->where('user_id', Auth::user()->id)->exists()) {
            return redirect('dashboard');
        }
        return $next($request);
    }
}
