<?php

namespace App\Mail;

use App\Models\Wishlist;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SharedWishlistMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    private $wishlist;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Wishlist $wishlist)
    {
        $this->wishlist = $wishlist;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $wishlistUrl = url('shared-wishlists/' . $this->wishlist->slug);
        return $this->markdown('mail.wishlist.share', [
            'url' => $wishlistUrl
        ])
            ->subject('A Wishlist was shared with you!');
    }
}
