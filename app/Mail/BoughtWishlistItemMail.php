<?php

namespace App\Mail;

use App\Models\WishlistItem;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class BoughtWishlistItemMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    private $wishlistItem;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(WishlistItem $wishlistItem)
    {
        $this->wishlistItem = $wishlistItem;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $wishlistUrl = url('shared-wishlists/' . $this->wishlistItem->wishlist->slug);
        return $this->markdown('mail.wishlist.boughtItem', [
            'url' => $wishlistUrl,
            'wishlistItem' => $this->wishlistItem
        ])
            ->subject('Confirmation for bought wishlist item');
    }
}
