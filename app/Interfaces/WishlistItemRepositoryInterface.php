<?php

namespace App\Interfaces;

interface WishlistItemRepositoryInterface
{
    public function getWishlistItems($wishlistId);

    public function getWishlistItem($wishlistItemId);

    public function createWishlistItem(array $data);

    public function updateWishlistItem($wishlistItemId, $data);

    public function deleteWishlistItem($wishlistItemId);
}
