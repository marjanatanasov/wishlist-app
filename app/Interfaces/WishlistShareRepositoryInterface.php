<?php

namespace App\Interfaces;

interface WishlistShareRepositoryInterface
{
    public function createWishlistShare(array $data);

    public function deleteWishlistShare($wishlistShareId);
}
