<?php

namespace App\Interfaces;

interface WishlistRepositoryInterface
{
    public function getWishlistsForUser($userId);

    public function getSharedWishlistsForUser($userId);

    public function getWishlist($wishlistId);

    public function getWishlistBySlug($wishlistId);

    public function getWishlistWithItemsBySlug($wishlistId);

    public function createWishlist(array $data);

    public function updateWishlist($wishlistId, $data);

    public function deleteWishlist($wishlistId);
}
