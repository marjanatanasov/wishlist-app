<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', 'App\Http\Controllers\Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'App\Http\Controllers\Auth\LoginController@login');

Route::get('register', 'App\Http\Controllers\Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'App\Http\Controllers\Auth\RegisterController@register');

Route::middleware('auth')->group(function() {
    Route::get('', 'App\Http\Controllers\DashboardController@index');
    Route::get('dashboard', 'App\Http\Controllers\DashboardController@index');
    Route::get('logout', 'App\Http\Controllers\Auth\UserLoginController@logout')->name('user.logout');

    Route::get('imgur/search/{term?}', 'App\Http\Controllers\ImgurSearchController@index');

    Route::resource('wishlists', 'App\Http\Controllers\WishlistController');
    Route::resource('wishlists/{wishlist}/items', 'App\Http\Controllers\WishlistItemController');
    Route::resource('wishlists/{wishlist}/shares', 'App\Http\Controllers\WishlistShareController')->only('create', 'store', 'destroy');

    Route::get('shared-wishlists/buy-item/{wishlistItem}', 'App\Http\Controllers\SharedWishlistsController@buyItem');
    Route::get('shared-wishlists/view-item/{wishlistItem}', 'App\Http\Controllers\SharedWishlistsController@viewItem');
    Route::resource('shared-wishlists', 'App\Http\Controllers\SharedWishlistsController')->only('index', 'show');
});
