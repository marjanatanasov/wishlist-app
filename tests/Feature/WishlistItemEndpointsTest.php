<?php


use App\Models\User;
use Tests\TestCase;

class WishlistItemEndpointsTest extends TestCase
{

    public function test_get_wishlist_items_should_return_ok()
    {
        $user = User::factory()->create();
        $wishlist = \App\Models\Wishlist::factory()->create(['user_id' => $user->id]);
        $response = $this->actingAs($user)->get('/wishlists/'.$wishlist->id.'/items');
        $response->assertStatus(200);
    }

    public function test_create_wishlist_item_should_return_ok()
    {
        $user = User::factory()->create();
        $wishlist = \App\Models\Wishlist::factory()->create(['user_id' => $user->id]);
        $response = $this->actingAs($user)->get('/wishlists/'.$wishlist->id.'/items/create');
        $response->assertStatus(200);
    }

    public function test_edit_wishlist_item_should_return_ok()
    {
        $user = User::factory()->create();
        $wishlist = \App\Models\Wishlist::factory()->create(['user_id' => $user->id]);
        $wishlistItem = \App\Models\WishlistItem::factory()->create(['wishlist_id' => $wishlist->id]);
        $response = $this->actingAs($user)->get('/wishlists/'.$wishlist->id.'/items/'.$wishlistItem->id.'/edit');
        $response->assertStatus(200);
    }

    public function test_destroy_wishlist_item_should_delete_and_return_ok()
    {
        $user = User::factory()->create();
        $wishlist = \App\Models\Wishlist::factory()->create(['user_id' => $user->id]);
        $wishlistItem = \App\Models\WishlistItem::factory()->create(['wishlist_id' => $wishlist->id]);
        $response = $this->actingAs($user)->delete('/wishlists/'.$wishlist->id.'/items/'.$wishlistItem->id);
        $response->assertStatus(200);
    }

    public function test_get_wishlist_items_should_redirect_if_unauthenticated()
    {
        $wishlist = \App\Models\Wishlist::factory()->create();
        $response = $this->get('/wishlists/'.$wishlist.'/items');
        $response->assertStatus(302)->assertRedirect('login');
    }

    public function test_create_wishlist_should_redirect_if_unauthenticated()
    {
        $wishlist = \App\Models\Wishlist::factory()->create();
        $response = $this->get('/wishlists/'.$wishlist->id.'/items/create');
        $response->assertStatus(302)->assertRedirect('login');
    }

    public function test_edit_wishlist_should_redirect_if_unauthenticated()
    {
        $wishlist = \App\Models\Wishlist::factory()->create();
        $wishlistItem = \App\Models\WishlistItem::factory()->create(['wishlist_id' => $wishlist->id]);
        $response = $this->get('/wishlists/'.$wishlist->id.'/items/'.$wishlistItem->id.'/edit');
        $response->assertStatus(302)->assertRedirect('login');
    }

    public function test_delete_wishlist_should_redirect_if_unauthenticated()
    {
        $wishlist = \App\Models\Wishlist::factory()->create();
        $wishlistItem = \App\Models\WishlistItem::factory()->create(['wishlist_id' => $wishlist->id]);
        $response = $this->delete('/wishlists/'.$wishlist->id.'/items/'.$wishlistItem->id);
        $response->assertStatus(302)->assertRedirect('login');
    }

    public function test_create_wishlist_item_should_redirect_if_doesnt_belong_to_logged_user()
    {
        $user = User::factory()->create();
        $wishlist = \App\Models\Wishlist::factory()->create();
        $response = $this->actingAs($user)->get('/wishlists/'.$wishlist->id.'/items/create');
        $response->assertStatus(302)->assertRedirect('dashboard');
    }

    public function test_edit_wishlist_item_should_redirect_if_doesnt_belong_to_logged_user()
    {
        $user = User::factory()->create();
        $wishlist = \App\Models\Wishlist::factory()->create();
        $wishlistItem = \App\Models\WishlistItem::factory()->create(['wishlist_id' => $wishlist->id]);
        $response = $this->actingAs($user)->get('/wishlists/'.$wishlist->id.'/items/'.$wishlistItem->id.'/edit');
        $response->assertStatus(302)->assertRedirect('dashboard');
    }

}
