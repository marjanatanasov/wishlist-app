<?php


use App\Models\User;
use Tests\TestCase;

class WishlistEndpointsTest extends TestCase
{

    public function test_get_wishlists_should_return_ok()
    {
        $user = User::factory()->create();
        $response = $this->actingAs($user)->get('/wishlists');
        $response->assertStatus(200);
    }

    public function test_create_wishlist_should_return_ok()
    {
        $user = User::factory()->create();
        $response = $this->actingAs($user)->get('/wishlists/create');
        $response->assertStatus(200);
    }

    public function test_edit_wishlist_should_return_ok()
    {
        $user = User::factory()->create();
        $wishlist = \App\Models\Wishlist::factory()->create(['user_id' => $user->id]);
        $response = $this->actingAs($user)->get('/wishlists/'.$wishlist->id.'/edit');
        $response->assertStatus(200);
    }

    public function test_destroy_wishlist_should_delete_and_return_ok()
    {
        $user = User::factory()->create();
        $wishlist = \App\Models\Wishlist::factory()->create(['user_id' => $user->id]);
        $response = $this->actingAs($user)->delete('/wishlists/'.$wishlist->id);
        $response->assertStatus(200);
    }

    public function test_edit_wishlist_should_redirect_if_doesnt_belong_to_logged_user()
    {
        $user = User::factory()->create();
        $wishlist = \App\Models\Wishlist::factory()->create();
        $response = $this->actingAs($user)->get('/wishlists/'.$wishlist->id.'/edit');
        $response->assertStatus(302)->assertRedirect('dashboard');
    }

    public function test_get_wishlists_should_redirect_if_unauthenticated()
    {
        $response = $this->get('/wishlists');
        $response->assertStatus(302)->assertRedirect('login');
    }

    public function test_create_wishlist_should_redirect_if_unauthenticated()
    {
        $response = $this->get('/wishlists/create');
        $response->assertStatus(302)->assertRedirect('login');
    }

    public function test_edit_wishlist_should_redirect_if_unauthenticated()
    {
        $wishlist = \App\Models\Wishlist::factory()->create();
        $response = $this->get('/wishlists/'.$wishlist->id.'/edit');
        $response->assertStatus(302)->assertRedirect('login');
    }

    public function test_delete_wishlist_should_redirect_if_unauthenticated()
    {
        $wishlist = \App\Models\Wishlist::factory()->create();
        $response = $this->delete('/wishlists/'.$wishlist->id);
        $response->assertStatus(302)->assertRedirect('login');
    }
}
