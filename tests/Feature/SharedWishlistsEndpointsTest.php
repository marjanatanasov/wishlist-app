<?php


use App\Models\User;
use App\Models\Wishlist;
use App\Models\WishlistItem;
use App\Models\WishlistShare;
use Tests\TestCase;

class SharedWishlistsEndpointsTest extends TestCase
{

    public function test_get_shared_wishlists_should_return_ok()
    {
        $user = User::factory()->create();
        $response = $this->actingAs($user)->get('/shared-wishlists');
        $response->assertStatus(200);
    }

    public function test_get_shared_wishlists_should_redirect_if_unauthenticated()
    {
        $response = $this->get('/shared-wishlists');
        $response->assertStatus(302)->assertRedirect('login');
    }


    public function test_view_shared_wishlist_should_return_ok()
    {
        $user = User::factory()->create();
        $wishlist = Wishlist::factory()->create();
        WishlistShare::factory()->create(['user_id' => $user->id, 'wishlist_id' => $wishlist->id]);
        $response = $this->actingAs($user)->get('/shared-wishlists/'.$wishlist->slug);
        $response->assertStatus(200);
    }

    public function test_view_shared_wishlist_should_redirect_if_not_shared_with_user()
    {
        $user = User::factory()->create();
        $wishlist = Wishlist::factory()->create();
        $response = $this->actingAs($user)->get('/shared-wishlists/'.$wishlist->slug);
        $response->assertStatus(302)->assertRedirect('dashboard');
    }

    public function test_view_shared_wishlist_should_redirect_if_unauthenticated()
    {
        $wishlist = Wishlist::factory()->create();
        $response = $this->get('/shared-wishlists/'.$wishlist->slug);
        $response->assertStatus(302)->assertRedirect('login');
    }




    public function test_view_shared_wishlist_item_should_return_ok()
    {
        $user = User::factory()->create();
        $wishlist = Wishlist::factory()->create();
        $wishlistItem = WishlistItem::factory()->create(['wishlist_id' => $wishlist->id]);
        WishlistShare::factory()->create(['user_id' => $user->id, 'wishlist_id' => $wishlist->id]);
        $response = $this->actingAs($user)->get('/shared-wishlists/view-item/'.$wishlistItem->id);
        $response->assertStatus(200);
    }

    public function test_view_shared_wishlist_item_should_redirect_if_not_shared_with_user()
    {
        $user = User::factory()->create();
        $wishlistItem = WishlistItem::factory()->create();
        $response = $this->actingAs($user)->get('/shared-wishlists/view-item/'.$wishlistItem->id);
        $response->assertStatus(302)->assertRedirect('dashboard');
    }

    public function test_view_shared_wishlist_item_should_redirect_if_unauthenticated()
    {
        $wishlistItem = WishlistItem::factory()->create();
        $response = $this->get('/shared-wishlists/view-item/'.$wishlistItem->id);
        $response->assertStatus(302)->assertRedirect('login');
    }



    public function test_buy_shared_wishlist_item_should_redirect_if_unauthenticated()
    {
        $wishlistItem = WishlistItem::factory()->create();
        $response = $this->get('/shared-wishlists/buy-item/'.$wishlistItem->id);
        $response->assertStatus(302)->assertRedirect('login');
    }

    public function test_buy_shared_wishlist_item_should_return_ok()
    {
        $user = User::factory()->create();
        $wishlist = Wishlist::factory()->create();
        $wishlistItem = WishlistItem::factory()->create(['wishlist_id' => $wishlist->id]);
        WishlistShare::factory()->create(['user_id' => $user->id, 'wishlist_id' => $wishlist->id]);
        $response = $this->actingAs($user)->get('/shared-wishlists/buy-item/'.$wishlistItem->id);
        $response->assertStatus(302)->assertRedirect(route('shared-wishlists.show', $wishlist->slug));
    }

    public function test_buy_shared_wishlist_item_should_redirect_if_not_shared_with_user()
    {
        $user = User::factory()->create();
        $wishlistItem = WishlistItem::factory()->create();
        $response = $this->actingAs($user)->get('/shared-wishlists/buy-item/'.$wishlistItem->id);
        $response->assertStatus(302)->assertRedirect('dashboard');
    }
}
