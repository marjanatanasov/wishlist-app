<?php


use App\Models\User;
use Tests\TestCase;

class AuthEndpointsTest extends TestCase
{

    public function test_redirect_from_login_to_dashboard_if_authenticated()
    {
        $user = User::factory()->create();
        $response = $this->actingAs($user)->get('/login');
        $response->assertStatus(302)->assertRedirect('dashboard');
    }

    public function test_redirect_from_dashboard_to_login_if_unauthenticated()
    {
        $response = $this->get('/dashboard');
        $response->assertStatus(302)->assertRedirect('login');
    }

    public function test_redirect_to_login_after_logout()
    {
        $response = $this->get('/logout');
        $response->assertStatus(302)->assertRedirect('login');
    }

}
