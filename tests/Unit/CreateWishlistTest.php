<?php

namespace Tests\Unit;

use App\Models\User;
use App\Models\Wishlist;
use Tests\TestCase;

class CreateWishlistTest extends TestCase
{

    public function test_can_create_wishlist_if_authenticated()
    {
        $user = User::factory()->create();
        $response = $this->actingAs($user)->post('/wishlists', [
            'name' => 'test'
        ]);
        $response->assertStatus(302)->assertRedirect('wishlists');
        $this->assertTrue(
            Wishlist::query()->where('name', 'test')
                ->where('user_id', $user->id)
                ->exists()
        );
    }

    public function test_will_show_error_if_name_missing()
    {
        $user = User::factory()->create();
        $response = $this->actingAs($user)->post('/wishlists');
        $response->assertSessionHas('errors');
    }
}
