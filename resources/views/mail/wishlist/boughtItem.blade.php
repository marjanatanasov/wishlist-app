@component('mail::message')
# Bough Wishlist Item

This is a confirmation that you have agreed to buy this wishlist item

Name: {{ $wishlistItem->name }}<br/>
Where to buy: {{ $wishlistItem->url }}

@component('mail::button', ['url' => $url, 'color' => 'success'])
    View Wishlist
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
