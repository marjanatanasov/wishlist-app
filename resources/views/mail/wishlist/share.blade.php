@component('mail::message')
# Shared Wishlist

A Wishlist was shared with you!

@component('mail::button', ['url' => $url, 'color' => 'success'])
    View Wishlist
@endcomponent

Log in or sign up and start making wishes come true!<br/>
Thanks,<br>
{{ config('app.name') }}
@endcomponent
