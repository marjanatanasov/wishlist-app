@extends('layouts.login')

@section('content')
    <form class="form-horizontal" method="POST" action="{{ url('login') }}">
        {{ csrf_field() }}
        <h1 class="h3 mb-3 fw-normal">Please sign in</h1>

        <div class="form-floating">
            <input type="email" class="form-control" id="floatingInput" placeholder="name@example.com" name="email">
            <label for="floatingInput">Email address</label>
        </div>
        <div class="form-floating">
            <input type="password" class="form-control" id="floatingPassword" placeholder="Password" name="password">
            <label for="floatingPassword">Password</label>
        </div>
        @if ($errors->has('email'))
            <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
        @endif
        <button class="w-100 btn btn-lg btn-primary" type="submit">Sign in</button>
        <a href="{{ route('register') }}">Register</a>
    </form>
@endsection
