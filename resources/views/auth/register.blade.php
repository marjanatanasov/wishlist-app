@extends('layouts.login')

@section('content')
    <form class="form-horizontal" method="POST" action="{{ url('register') }}">
        {{ csrf_field() }}
        <h1 class="h3 mb-3 fw-normal">Enter your details to sign up</h1>

        <div class="form-floating">
            <input type="text" class="form-control" id="floatingInput" placeholder="Your Name" name="name">
            <label for="floatingInput">Your Name</label>
        </div>
        <div class="form-floating">
            <input type="email" class="form-control" id="floatingInput" placeholder="name@example.com" name="email">
            <label for="floatingInput">Email address</label>
        </div>
        <div class="form-floating">
            <input type="password" class="form-control" id="floatingPassword" placeholder="Password" name="password">
            <label for="floatingPassword">Password</label>
        </div>
        <div class="form-floating">
            <input type="password" class="form-control" id="floatingPassword" placeholder="Confirm Password"
                   name="password_confirmation">
            <label for="floatingPassword">Confirm Password</label>
        </div>
        @if ($errors->has('name'))
            <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
        @endif
        @if ($errors->has('email'))
            <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
        @endif
        @if ($errors->has('password'))
            <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
        @endif
        <button class="w-100 btn btn-lg btn-primary" type="submit">Register</button>
        <a href="{{ route('login') }}">Login</a>
    </form>
@endsection
