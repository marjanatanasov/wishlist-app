@extends('layouts.app')

@section('title')
    Edit Wishlist
@endsection

@section('content')
    <ul class="nav nav-tabs" id="myTab">
        <li class="nav-item">
            <a href="#settings" class="nav-link active" data-bs-toggle="tab">Settings</a>
        </li>
        <li class="nav-item">
            <a href="#items" class="nav-link" data-bs-toggle="tab">Items</a>
        </li>
        <li class="nav-item">
            <a href="#shares" class="nav-link" data-bs-toggle="tab">Shares</a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane fade show active pt-5" id="settings">
            <form class="form-horizontal" method="POST" action="{{ route('wishlists.update', $wishlist->id) }}">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="put"/>
                <div class="mb-3">
                    <label for="name" class="form-label">Name</label>
                    <input type="text" class="form-control" id="name" name="name" value="{{ $wishlist->name }}">
                    @if ($errors->has('name'))
                        <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
                    @endif
                </div>

                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{{ route('wishlists.index') }}">
                    <button type="button" class="btn btn-success">Back</button>
                </a>
            </form>
        </div>
        <div class="tab-pane fade pt-5" id="items">
            @if(count($wishlistItems))
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col" width="10%">#</th>
                        <th scope="col" width="25%">Name</th>
                        <th scope="col" width="40%">Description</th>
                        <th scope="col" width="10%">Price</th>
                        <th scope="col" width="15%">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($wishlistItems as $wishlistItem)
                        <tr id="listtr-{{$wishlistItem->id}}">
                            <th scope="row">{{ $loop->iteration }}</th>
                            <td>{{ $wishlistItem->name }}</td>
                            <td>{{ $wishlistItem->description }}</td>
                            <td>{{ $wishlistItem->price }}</td>
                            <td>
                                <a href="{{ route('items.edit', [$wishlist->id, $wishlistItem->id]) }}">
                                    <button type="button" class="btn btn-primary btn-sm">Edit</button>
                                </a>
                                <button type="button" class="btn btn-danger btn-sm deleteitem"
                                        data-wishlistId="{{ $wishlist->id }}"
                                        data-wishlistItemId="{{ $wishlistItem->id }}"
                                        data-token="{{csrf_token()}}">Delete
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="d-flex justify-content-center">
                    {!! $wishlistItems->fragment('items')->render() !!}
                </div>
            @else
                <p class="fs-5 text-muted text-center">You don't have any wishlist items.</p>
            @endif
            <a href="{{ route('items.create', $wishlist->id) }}">
                <button type="button" class="btn btn-success">Create Wishlist Item</button>
                <a href="{{ route('wishlists.index') }}">
                    <button type="button" class="btn btn-success">Back</button>
                </a>
            </a>
        </div>
        <div class="tab-pane fade pt-5" id="shares">
            <h4 class="mt-2">Shares</h4>
            <p>Share would consider as accepted once the email that it's sent to exists in the Users table.</p>
            <table class="table">
                <thead>
                <tr>
                    <th scope="col" width="10%">#</th>
                    <th scope="col" width="50%">Email</th>
                    <th scope="col" width="20%">Accepted</th>
                    <th scope="col" width="20%">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($wishlist->shares as $share)
                    <tr id="listtr-{{ $share->id }}">
                        <th scope="row">{{ $loop->iteration }}</th>
                        <td>{{ $share->user->email ?? $share->email }}</td>
                        <td>{{ !empty($share->user) ? 'Yes' : 'No' }}</td>
                        <td>
                            <button type="button" class="btn btn-danger btn-sm deleteshare"
                                    data-wishlistid="{{ $wishlist->id }}"
                                    data-id="{{ $share->id }}"
                                    data-token="{{csrf_token()}}">Delete
                            </button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <a href="{{ route('shares.create', $wishlist->id) }}">
                <button type="button" class="btn btn-primary">Create Wishlist Share</button>
            </a>
            <a href="{{ route('wishlists.index') }}">
                <button type="button" class="btn btn-success">Back</button>
            </a>

        </div>
    </div>
@endsection

@section('customjs')
    <script src="{{asset('js/user/wishlists.js')}}"></script>
    <script src="{{asset('js/user/wishlistItems.js')}}"></script>
@stop
