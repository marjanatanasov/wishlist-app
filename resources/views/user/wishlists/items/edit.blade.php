@extends('layouts.app')

@section('title')
    Edit Wishlist Item
@endsection

@section('content')
    <form class="form-horizontal" method="POST" action="{{ route('items.update', [$wishlist->id, $wishlistItem->id]) }}"
          enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="put"/>
        <div class="mb-3">
            <label for="name" class="form-label">Name</label>
            <input type="text" class="form-control" id="name" name="name" value="{{ $wishlistItem->name }}">
            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>
        <div class="mb-3">
            <label for="description" class="form-label">Description</label>
            <textarea class="form-control" id="description"
                      name="description">{{ $wishlistItem->description }}</textarea>
            @if ($errors->has('description'))
                <span class="help-block">
                    <strong>{{ $errors->first('description') }}</strong>
                </span>
            @endif
        </div>
        <div class="mb-3">
            <label for="price" class="form-label">Price</label>
            <input type="text" class="form-control" id="price" name="price" value="{{ $wishlistItem->price }}">
            @if ($errors->has('price'))
                <span class="help-block">
                    <strong>{{ $errors->first('price') }}</strong>
                </span>
            @endif
        </div>
        <div class="mb-3">
            <label for="url" class="form-label">URL</label>
            <input type="text" class="form-control" id="url" name="url" value="{{ $wishlistItem->url }}">
            @if ($errors->has('url'))
                <span class="help-block">
                    <strong>{{ $errors->first('url') }}</strong>
                </span>
            @endif
        </div>
        @if(!empty($wishlistItem->image))
            <div class="mb-3">
                <label for="image" class="form-label">Existing Image</label><br/>
                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <img src="{{ $wishlistItem->image }}" class="imgur-preview" alt="preview image"/>
                    </div>
                </div>
            </div>
        @endif
        <div class="mb-3">
            <label for="image" class="form-label">Upload Image</label>
            <input type="file" class="form-control" id="image" name="image">
            <input type="hidden" name="imgur_image" id="imgur_image" value="">
            @if ($errors->has('image'))
                <span class="help-block">
                    <strong>{{ $errors->first('image') }}</strong>
                </span>
            @endif
            <h6>OR</h6>
            <label class="form-label">Select from Imgur (click on an image to select)</label>
            <div class="row hidden" id="imgurImagesWrapper">

            </div>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
        <a href="{{ route('wishlists.edit', $wishlist->id) }}">
            <button type="button" class="btn btn-success">Back</button>
        </a>
    </form>
@endsection

@section('customjs')
    <script src="{{asset('js/user/wishlistItems.js')}}"></script>
    <script>
        $('#name').trigger('keyup');
    </script>
@stop

