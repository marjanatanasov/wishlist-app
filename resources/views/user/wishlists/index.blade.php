@extends('layouts.app')

@section('title')
    My Wishlists
@endsection

@section('content')
    @if(count($wishlists))
        <table class="table">
            <thead>
            <tr>
                <th scope="col" width="10%">#</th>
                <th scope="col" width="70%">Name</th>
                <th scope="col" width="20%">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($wishlists as $wishlist)
                <tr id="listtr-{{ $wishlist->id }}">
                    <th scope="row">{{ $loop->iteration }}</th>
                    <td>{{ $wishlist->name }}</td>
                    <td>
                        <a href="{{ url('wishlists/'.$wishlist->id.'/edit') }}">
                            <button type="button" class="btn btn-primary btn-sm">Edit</button>
                        </a>
                        @csrf
                        @method('DELETE')
                        <button type="button" class="btn btn-danger btn-sm deletewishlist"
                                data-id="{{ $wishlist->id }}"
                                data-token="{{csrf_token()}}">Delete
                        </button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <p class="fs-5 text-muted text-center">You don't have any wishlists.</p>
    @endif
    <a href="{{ url('wishlists/create') }}">
        <button type="button" class="btn btn-success">Create Wishlist</button>
    </a>
@endsection

@section('customjs')
    <script src="{{asset('js/user/wishlists.js')}}"></script>
@stop
