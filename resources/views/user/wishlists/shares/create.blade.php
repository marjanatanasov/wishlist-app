@extends('layouts.app')

@section('title')
    Create Wishlist Share for {{ $wishlist->name }}
@endsection

@section('content')
    <form class="form-horizontal" method="POST" action="{{ route('shares.store', $wishlist->id) }}">
        {{ csrf_field() }}
        <div class="mb-3">
            <label for="email" class="form-label">Email</label>
            <input type="email" class="form-control" id="email" name="email">
            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
        <a href="{{ route('wishlists.edit', $wishlist->id) }}">
            <button type="button" class="btn btn-success">Back</button>
        </a>
    </form>
@endsection

