@extends('layouts.app')

@section('title')
    Home
@endsection

@section('content')
    <p class="fs-5 text-muted text-center">Welcome to the Wishlist App. May your wishes come true! :) </p>
@endsection
