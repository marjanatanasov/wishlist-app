@extends('layouts.app')

@section('title')
    View Shared Wishlist - {{ $wishlist->name }}
@endsection

@section('content')
    @if(count($wishlist->items))
        <table class="table">
            <thead>
            <tr>
                <th scope="col" width="10%">#</th>
                <th scope="col" width="40%">Name</th>
                <th scope="col" width="15%">Price</th>
                <th scope="col" width="15%">Is bought by me</th>
                <th scope="col" width="20%">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($wishlist->items as $wishlistItem)
                <tr class="{{ $wishlistItem->is_bought ? 'red-bg' : 'green-bg' }}">
                    <th scope="row">{{ $loop->iteration }}</th>
                    <td>{{ $wishlistItem->name }}</td>
                    <td>{{ $wishlistItem->price }}</td>
                    <td>{{ $wishlistItem->bought_by == auth()->user()->id ? 'Yes' : '' }}</td>
                    <td>
                        @if(!$wishlistItem->is_bought)
                            <a href="{{ url('shared-wishlists/view-item/'.$wishlistItem->id) }}">
                                <button type="button" class="btn btn-primary btn-sm">View Item</button>
                            </a>
                            <a href="{{ url('shared-wishlists/buy-item/'.$wishlistItem->id) }}">
                                <button type="button" class="btn btn-success btn-sm">I will buy this</button>
                            </a>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <p class="fs-5 text-muted text-center">No wishlist items found.</p>
    @endif
@endsection
