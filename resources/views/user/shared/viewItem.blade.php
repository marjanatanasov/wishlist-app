@extends('layouts.app')

@section('title')
    View Shared Wishlist Item
@endsection

@section('content')
    <div class="mt-3">
        <label>Name</label>
        <h4>{{ $wishlistItem->name }}</h4>
    </div>
    <div class="mt-3">
        <label>Description</label>
        <h5>{{ $wishlistItem->description }}</h5>
    </div>
    <div class="mt-3">
        <label>Price</label>
        <h5>{{ $wishlistItem->price }}</h5>
    </div>
    <div class="mt-3">
        <label>Where to buy</label>
        <h5>{{ $wishlistItem->url ?? '' }}</h5>
    </div>
    <div class="mt-3">
        <label>Image</label>
        @if(!empty($wishlistItem->image))
            <div class="mb-3">
                <label for="image" class="form-label">Existing Image</label><br/>
                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <img src="{{ $wishlistItem->image }}" class="imgur-preview" alt="preview image"/>
                    </div>
                </div>
            </div>
        @endif
    </div>
    <div class="mt-3">
        <a href="{{ url('shared-wishlists/buy-item/' . $wishlistItem->id) }}">
            <button type="button" class="btn btn-success">I will buy this</button>
        </a>
        <a href="{{ route('shared-wishlists.show', $wishlistItem->wishlist->slug) }}">
            <button type="button" class="btn btn-primary">Back</button>
        </a>
    </div>
@endsection
