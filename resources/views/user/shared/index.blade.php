@extends('layouts.app')

@section('title')
    Shared Wishlists
@endsection

@section('content')
    @if(count($wishlistShares))
        <table class="table">
            <thead>
            <tr>
                <th scope="col" width="10%">#</th>
                <th scope="col" width="45%">Name</th>
                <th scope="col" width="25%">Shared By</th>
                <th scope="col" width="20%">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($wishlistShares as $wishlistShare)
                <tr id="listtr-{{ $wishlistShare->wishlist->id }}">
                    <th scope="row">{{ $loop->iteration }}</th>
                    <td>{{ $wishlistShare->wishlist->name }}</td>
                    <td>{{ $wishlistShare->wishlist->user->name }}</td>
                    <td>
                        <a href="{{ url('shared-wishlists/'.$wishlistShare->wishlist->slug) }}">
                            <button type="button" class="btn btn-primary btn-sm">View</button>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <p class="fs-5 text-muted text-center">You don't have any shared wishlists.</p>
    @endif
@endsection
