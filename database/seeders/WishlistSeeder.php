<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class WishlistSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Wishlist::factory(100)->create();
    }
}
