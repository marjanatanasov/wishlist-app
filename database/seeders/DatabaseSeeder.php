<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(WishlistSeeder::class);
        $this->call(WishlistItemSeeder::class);
        $this->call(WishlistShareSeeder::class);
    }
}
