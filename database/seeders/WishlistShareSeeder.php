<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class WishlistShareSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\WishlistShare::factory(200)->create();
    }
}
