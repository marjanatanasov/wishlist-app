<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class WishlistItemSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\WishlistItem::factory(2000)->create();
    }
}
