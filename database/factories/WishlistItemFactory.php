<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class WishlistItemFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $wishlistIds = DB::table('wishlists')->pluck('id');
        $userIds = DB::table('users')->pluck('id');
        $is_bought = $this->faker->boolean;

        return [
            'wishlist_id' => $this->faker->randomElement($wishlistIds),
            'name' => $this->faker->word(),
            'description' => $this->faker->text(),
            'price' => $this->faker->numberBetween(1, 1000),
            'image' => $this->faker->imageUrl(),
            'url' => $this->faker->url,
            'is_bought' => $is_bought,
            'bought_by' => $is_bought ? $this->faker->randomElement($userIds) : null
        ];
    }
}
