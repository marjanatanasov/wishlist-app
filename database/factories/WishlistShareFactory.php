<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class WishlistShareFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $wishlistIds = DB::table('wishlists')->pluck('id');
        $userIds = DB::table('users')->pluck('id');

        return [
            'wishlist_id' => $this->faker->randomElement($wishlistIds),
            'user_id' => $this->faker->randomElement($userIds),
        ];
    }
}
