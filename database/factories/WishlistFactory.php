<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class WishlistFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $userIds = DB::table('users')->pluck('id');

        return [
            'user_id' => $this->faker->randomElement($userIds),
            'name' => $this->faker->word(),
            'slug' => $this->faker->slug(),
        ];
    }
}
